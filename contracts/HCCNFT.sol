//Contract based on [https://docs.openzeppelin.com/contracts/3.x/erc721](https://docs.openzeppelin.com/contracts/3.x/erc721)
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract HCCNFT is ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    mapping(address => string) public whitelistedTokenURIs;

    modifier onlyWhitelistedOrOwner() {
        require(
            keccak256(abi.encodePacked((whitelistedTokenURIs[msg.sender]))) !=
                keccak256(abi.encodePacked((""))) ||
                msg.sender == owner(),
            "Forbidden action."
        );
        _;
    }

    constructor(
        address[] memory whitelists,
        string[] memory whitelistsTokenURIs
    ) public ERC721("Highrollers Casino Club", "HCC") {
        uint256 i = 0;
        for (i; i < whitelists.length; i++) {
            whitelistedTokenURIs[whitelists[i]] = whitelistsTokenURIs[i];
        }
    }

    function getTokenURIForAddress(address _addr)
        public
        onlyWhitelistedOrOwner
        returns (string memory)
    {
        require(_addr != address(0), "Invalid whitelist address provided.");

        return whitelistedTokenURIs[_addr];
    }

    function mintNFT(address recipient, string memory tokenURI)
        public
        onlyWhitelistedOrOwner
        returns (uint256)
    {
        _tokenIds.increment();

        uint256 newItemId = _tokenIds.current();
        _mint(recipient, newItemId);
        _setTokenURI(newItemId, tokenURI);

        return newItemId;
    }
}

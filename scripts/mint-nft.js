// Work in progress
require("dotenv/config");
const ALCHEMY_API_URL = process.env.ALCHEMY_API_URL;
const ALCHEMY_PUBLIC_KEY = process.env.ALCHEMY_PUBLIC_KEY;
const { createAlchemyWeb3 } = require("@alch/alchemy-web3");
const web3 = createAlchemyWeb3(ALCHEMY_API_URL);

const contract = require("../artifacts/contracts/myNFT.sol/MyNFT.json");

const contractAddress = "0x72B38e53Ad7f02eA8b9E21e96e9eA9C731Ac0925";
const nftContract = new web3.eth.Contract(contract.abi, contractAddress);

async function checkWhitelistMapping() {
  let tokenUri = await nftContract.methods
    .getTokenURIForAddress("0xcbA04Fb622EDD2B2faB77b66877b1BFeF86a27e0")
    .call();
  console.log(tokenUri);
}

// async function mintNFT(tokenURI) {
//     const nonce = await web3.eth.getTransactionCount(ALCHEMY_PUBLIC_KEY, 'latest'); //get latest nonce

//   //the transaction
//     const tx = {
//       'from': ALCHEMY_PUBLIC_KEY,
//       'to': contractAddress,
//       'nonce': nonce,
//       'gas': 500000,
//       'data': nftContract.methods.mintNFT(ALCHEMY_PUBLIC_KEY, tokenURI).encodeABI()
//     };
//     const signPromise = web3.eth.accounts.signTransaction(tx, ALCHEMY_PRIVATE_KEY)

//     try{
//         let signedTx = await signPromise();
//         web3.eth.sendSignedTransaction(
//           signedTx.rawTransaction,
//           function (err, hash) {
//             if (!err) {
//               console.log(
//                 "The hash of your transaction is: ",
//                 hash,
//                 "\nCheck Alchemy's Mempool to view the status of your transaction!"
//               )
//             } else {
//               console.log(
//                 "Something went wrong when submitting your transaction:",
//                 err
//               )
//             }
//           }
//         );
//     }catch(err){
//         console.log(" Promise failed:", err);
//     }

//   }​

//   mintNFT(
//     "https://gateway.pinata.cloud/ipfs/QmYueiuRNmL4MiA2GwtVMm6ZagknXnSpQnB3z2gWbz36hP"
//   )

checkWhitelistMapping();

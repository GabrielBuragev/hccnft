/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require("dotenv").config();
require("@nomiclabs/hardhat-ethers");
const { ALCHEMY_API_URL, ALCHEMY_PRIVATE_KEY, PRIVATE_KEY_LOCAL } = process.env;
module.exports = {
  solidity: "0.8.0",
  defaultNetwork: "rinkeby",
  networks: {
    hardhat: {},
    rinkeby: {
      url: ALCHEMY_API_URL,
      accounts: [`0x${ALCHEMY_PRIVATE_KEY}`],
    },
    local: {
      url: "HTTP://127.0.0.1:7545",
      accounts: [`0x${PRIVATE_KEY_LOCAL}`],
    },
  },
};

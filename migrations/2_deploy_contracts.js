const HCCNFT = artifacts.require("HCCNFT");

module.exports = function (deployer) {
  deployer.deploy(
    HCCNFT,
    // Insert whitelist addresses ("free mint")
    ["0x853Ba9D59BcD4Ed8dB6B57b9B97AcE011406Fbd5"],
    ["https://facebook.com"]
  );
};
